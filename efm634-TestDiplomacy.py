#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
# ----
    # read
    # ----

    def test_read_1(self):
        s = ["A Austin hold","B Boston move Austin", "C Chicago support A"]
        army_info = diplomacy_read(s)
        self.assertEqual(army_info,  [["A", "Austin", "hold"],["B", "Boston", "move", "Austin"],["C", "Chicago", "support", "A"]])

    def test_read_2(self):
        s = ["B Boston move Austin", "A Austin hold", "C Chicago move Austin"]
        army_info = diplomacy_read(s)
        self.assertEqual(army_info,  [["B", "Boston", "move", "Austin"], ["A", "Austin", "hold"],["C", "Chicago", "move", "Austin"]])

    def test_read_3(self):
        s = ["C Chicago hold","B Boston hold","A Austin hold","D Denver support A"]
        army_info = diplomacy_read(s)
        self.assertEqual(army_info,  [["C", "Chicago", "hold"], ["B", "Boston", "hold"],["A", "Austin", "hold"], ["D", "Denver", "support", "A"]])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval((["A", "Austin", "hold"],["B", "Boston", "move", "Austin"],["C", "Chicago", "support", "A"]))
        self.assertEqual(v, ["A Austin", "B [dead]", "C Chicago"])
    
    def test_eval_2(self):
        v = diplomacy_eval((["C", "Chicago", "move", "Austin"],["A", "Austin", "hold"],["B", "Boston", "move", "Austin"]))
        self.assertEqual(v, ["A [dead]", "B [dead]", "C [dead]"])

    def test_eval_3(self):
        v = diplomacy_eval((["B", "Boston", "move", "Austin"],["A", "Austin", "move", "Chicago"],["C", "Chicago", "move", "Boston"]))
        self.assertEqual(v, ["A Chicago", "B Austin", "C Boston"])

    def test_eval_4(self):
        v = diplomacy_eval((["B", "Boston", "move", "Austin"],["A", "Austin", "support", "C"],["C", "Chicago", "hold"]))
        self.assertEqual(v, ["A [dead]", "B [dead]", "C Chicago"])

    def test_eval_5(self):
       v = diplomacy_eval((["A", "Austin", "hold"], ["B", "Boston", "move", "Austin"], ["C", "Chicago", "support", "A"], ["D", "Dallas", "support", "B"]))
       self.assertEqual(v, (["A [dead]", "B [dead]", "C Chicago", "D Dallas"]))

    def test_eval_6(self):
        v = diplomacy_eval((["A", "Austin", "move", "Chicago"], ["B", "Boston", "move", "Austin"], ["C", "Chicago", "hold"]))
        self.assertEqual(v, (["A [dead]", "B Austin", "C [dead]"]))

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ("A Chicago", "B Austin", "C Boston"))
        self.assertEqual(w.getvalue(), "A Chicago\nB Austin\nC Boston\n")
    
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ("A Austin", "B Boston", "C Chicago", "D Denver"))
        self.assertEqual(w.getvalue(), "A Austin\nB Boston\nC Chicago\nD Denver\n")
    
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ( "B [dead]", "C [dead]"))
        self.assertEqual(w.getvalue(), "B [dead]\nC [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Austin hold\nB Boston hold\nC Chicago hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Boston\nC Chicago\n")
        
    def test_solve_2(self):
        r = StringIO("A Austin hold\nB Boston move Austin\nC Chicago move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
        
    def test_solve_3(self):
        r = StringIO("A Austin hold\nB Boston move Austin\nC Chicago support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB [dead]\nC Chicago\n")
        
    def test_solve_4(self):
        r = StringIO("A Austin support B\n\nB Boston move Denver\nC Chicago support B\nD Denver hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Denver\nC Chicago\nD [dead]\n")
        
    def test_solve_5(self):
       r = StringIO("A Austin hold\nB Boston move Austin\nC Chicago support A\nD Dallas support A\n")
       w = StringIO()
       diplomacy_solve(r,w)
       self.assertEqual(
           w.getvalue(), "A Austin\nB [dead]\nC Chicago\nD Dallas\n")
       
# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()

