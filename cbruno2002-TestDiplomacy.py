#!/usr/bin/env python3

# -------------------------------
# git/diplomacy/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i,  "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, "Hold")

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i,  "B")
        self.assertEqual(j, "Barcelona")
        self.assertEqual(k, "Move")
        self.assertEqual(l, "Madrid")

    def test_read_3(self):
        s = "C London Support B\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i,  "C")
        self.assertEqual(j, "London")
        self.assertEqual(k, "Support")
        self.assertEqual(l, "B")



    # # -----
    # # print
    # # -----

    # def test_print_1(self):
    #     w = StringIO()
    #     diplomacy_print(w, "A", "Madrid")
    #     self.assertEqual(w.getvalue(), "A Madrid\n")

    # def test_print_2(self):
    #     w = StringIO()
    #     diplomacy_print(w, "B", "[dead]")
    #     self.assertEqual(w.getvalue(), "1 10 20\n")

    # def test_print_3(self):
    #     w = StringIO()
    #     diplomacy_print(w, "C", "London")
    #     self.assertEqual(w.getvalue(), "C London\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Dallas Support C\nF NYC Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE Dallas\nF NYC\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Dallas Support C\nF NYC Support D\nG Orlando Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD [dead]\nE Dallas\nF NYC\nG Orlando\n")

    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Dallas Support C\nF NYC Support D\nG Orlando Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD London\nE Dallas\nF NYC\nG Orlando\n")

    def test_solve6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\n")

    def test_solve7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move London\nC Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB London\nC [dead]\n")

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()
